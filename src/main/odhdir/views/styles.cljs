;; -*- eval: (rainbow-mode) -*-
(ns odhdir.views.styles
  "Instead of loading a .css file, use css-in-cljs to load these straight into the page.
  See https://tech.odhdir.com/2020/03/14/my-garden-css-has-ascended/"
  (:require [garden.core :as garden :refer [css] ]
            [garden.units :as u :refer [vh vw px em rem percent]]
            [garden.stylesheet :as style :refer [at-media]]))

(def contact
  "Contact front-page"
  [:.contact {:whitespace :nowrap}
   [:i {:margin-right (em 0.5)}]])

(def nav
  "Navbar style"
  [:nav#odh-nav
   [:.title {:margin-right (em 1)} ]
   [:.navbar-start
    [:.navbar-item {:font-size (rem 1.2)
                    :font-weight 900
                    :letter-spacing (em 0.1)}]]])

(def footer
  [:footer.footer
   [:.copyright-me {:float :left}]
   [:.foot-codelink {:float :right}]])

;; might be useful still
(def accordions
  "Styles for the accordions "
  [:div.zippy-container
   [:.goog-zippy-header {:margin-bottom 0}
    [:&:hover {:background "#Add8e6"
               :cursor :pointer}]
    [:&:after {:margin-left (em 1)
               :text-decoration :none} ]
    [:&.goog-zippy-collapsed:after {:content "\"↓\""}]
    [:&.goog-zippy-expanded:after {:content "\"↑\""}]]
   [:.goog-zippy-content {:transition-property :all
                          :transition-duration "0.5s"
                          :transition-timing-function :ease
                          :transition-delay "0.5s"}]])
   

(defn odhdir
  "Our stylesheet"
  []
  (css



   [:body
    [:.directory
     [:.filter-container
      [:ul {:height "32rem"
            :overflow "auto"
            :cursor "pointer"}]]
     [:.input-container {:margin-right "0.5rem"}]
     [:ul.directory-list
      [:li {:color "#8f8f8f"
            :display "grid"
            :grid-template-columns "6.5rem 11rem"}
       [:.image-container {:width "5em"
                           :height "7em"
                           :display "table-cell"
                           :list-style "none"
                           :vertical-align "top"}]
       [:.content  {:display "table-cell"
                    :padding "0"
                    :position "relative"}
        [:.link.specialty {:border-style "solid"
                           :border-width 0
                           :border-bottom-width (px 1)
                           :border-color "transparent"
                           :display "inline"}
         [:&:hover {:border-bottom-color "black"}]]
        [:a {:text-decoration "none"}
         [:h4 {:text-transform "none"
               :color "#002e5d"
               :font-size "1.6rem"
               :font-weight "bold"
               :margin "0"
               :font-family "\"HCo Ringside Narrow SSm\", \"Open Sans\", Helvetica, Arial, sans-serif"}]]
        [:.link {:font-size "1.06rem"
                 :color "#666"
                 :padding "0"
                 :position "relative"
                 :font-weight "500"}]
        [:.link.phone [:a {:color "#0062B8"}]]
        [:.link.email [:a {:color "#0062B8"}]]
        [:.link:first-of-type {:padding-left "0"}]
        [:.link:last-of-type {:border-right "none"}]

        [:.icon {:display "none"}]
        [:.cta {:position "absolute"
                :right "2rem"
                :top "1.25rem"}]]]]]
    
    [:ul.directory-list :.filterable {:display "grid"
                                      :position "relative"
                                      :grid-template-columns "repeat(3,minmax(1px,1fr))"
                                      :grid-gap "2.3rem 1rem"
                                      :padding "3.5rem 8rem"}]

    [:.directory-list
     [:.filterable-item
      [:.content
       [:h5 {:color "#333"
             :font-size "1rem"
             :line-height "1.235"
             :font-weight "500"
             :margin "0"}]]]]

    [:.statusfilter-container {:float "right"}
     [:.statusfilter {:font-size "125%"
                      :cursor "pointer"
                      :display "inline-block"}]
     [:.current {:text-decoration "underline"}]]

    [:.filter-container {:margin-bottom "2rem"
                         :position "relative"
                         :text-transform "uppercase"
                         :display "inline-block"
                         :min-width "300px"
                         :font-size "1.05rem"
                         :vertical-align "top"}
     [:span {:color "#86847d"
             :font-size "1.05rem"
             :margin-right "0.5rem"}]
     [:.filter-label {:background "#8f8f8f"
                      :width "auto"
                      :min-width "6.62rem"
                      :display "inline-block"
                      :vertical-align "middle"
                      :padding "1rem 2.62rem 0.85rem 0.75rem"
                      :position "relative"
                      :font-weight "bold"
                      :color "white"
                      :cursor "pointer"
                      :max-width "18em"}
      [:.icon {:position "absolute"
               :right "1rem"
               :font-size "1.05rem"
               :top "1.25em"}]]
     [:ul {:background "#8f8f8f"
           :max-width "20rem"
           :color "white"
           :font-weight "bold"
           :position "absolute"
           :top "3.5rem"
           :left "0"
           :z-index "2"
           :display "none"}
      [:li {:padding "1rem 1.3rem"}]
      [:li:hover {:background-color "#002f5d"}]]]


    [:.input-container {:display "inline-block"
                        :position "relative"
                        :padding "0.8rem 0.8rem 0.7rem 2.6rem"
                        :background "#8f8f8f"}]
    [:.input-containter:before {:font-family "byu-icons"
                                :content "\f002"
                                :color "white"
                                :position "absolute"
                                :top "14px"
                                :left "17px"}]


    [:.profile-content
     [:.text-content {:padding-left "2em"
                      :padding-top "1em"}]]
    [:.person
     [:h1 :h2 :h3 :h4
      [:a:hover {:color "#002e5d"}]]]
    [:.image-container
     [:img.directory-portrait {:display "block"
                               :height "100%"
                               :width "100%"
                               :object-fit "cover"}]]



    [:.single
     [:#header-image-main {:display "none"}]
     [:.image-container {:max-width "25%"
                         :display "inline-block"
                         :padding-left "1rem"
                         :padding-right "1rem"}]
     [:.profile-content
      [:img {:border-style "solid"
             :border-radius "0.75em"}]]
     [:#content {:margin-top "10rem"}]
     [:.content {:display "inline-block"
                 :vertical-align "top"
                 :max-width "70%"
                 :padding-bottom "2rem"}
      [:ul {:margin-left "1em"}]]]
    [:.personal-info-box {:margin-top "2rem"}]
    [:.research_areas_list
     [:a {:text-decoration "underline"}]]
    [:a.article-inner :.directory-list:hover
     [:.filterable-item:hover
      [:h4:hover {:color "#666667"}]]]

    [:.defaultIMG {:background-image "url(../wp-content/themes/wp-humanities-byu/resources/images/default_logo-hum.png)"
                   :background-repeat "no-repeat"
                   :background-size "100% 100%"}]

    [:.message.is-success
     [:.content {:color "green"
                 :font-weight 600}]]
    [:table
     [:.title {:white-space "nowrap"}]]
    accordions]
   

   (at-media
    {:screen true :max-width "1260px"}
    [:body
     [:.statusfilter-container {:float "none"}]])
   (at-media
    {:screen true :max-width "1250px"}
    [:body
     [:ul.directory-list :.filterable {:display "grid"
                                       :position "relative"
                                       :grid-template-columns "repeat(2,minmax(1px,1fr))"}]])
   (at-media
    {:screen true :max-width "975px"}
    [:body 
     [:.directory
      [:ul.directory-list
       [:li {:display "grid"}
        [:.content {:padding "1.25rem"}]
        [:.image-container {:display "inline-block"
                            :height "10rem"
                            :width "100%"}]]]]
     [:ul.directory-list :.filterable {:display "grid"
                                       :position "relative"
                                       :grid-template-columns "repeat(1,minmax(1px,.35fr))"
                                       :grid-gap "0.3rem 0.3rem"
                                       :padding "3.5rem 1.5rem"}]])
   (at-media
    {:screen true :max-width "699px"}
    [:body
     [:.directory
      [:ul.directory-list
       [:li
        [:.content {:padding "1.25rem"}
         [:h4 {:padding-bottom "1rem"
               :border-bottom "1px solid #b3b1a9"}]
         [:.link {:border "none"
                  :padding "0"
                  :display "block"
                  :margin-top "1rem"}]
         [:.cta {:position "static"
                 :margin-top "1.25rem"}]]]]]])
   (at-media
    {:screen true :max-width "599px"}
    [:body
     [:.statusfilter-container {:float "none"
                                :padding-bottom "1.5rem"}]
     [:.directory
      [:.input-container {:display "block"
                          :width "100%"
                          :margin-bottom "2%"}]
      [:.filter-container {:display "block"
                           :width "100%"}
       [:.filter-label {:display "block"
                        :width "100%"}]
       [:ul {:width "100%"
             :position "absolute"
             :max-height "80vh"}]]]])
   (at-media
    {:screen true :max-width "499px"}
    [:body
     [:.filter-container {:padding "0 2.5%"}
      [:ul {:left "2.5%"}]]
     [:.directory
      [:.input-container {:margin "0 auto 3%"
                          :width "95%"}]
      [:.filter-container {:padding "0 2.5%"}
       [:ul {:left "2.5%"}]]]])))

(defn clear-styles!
  "Remove existing style elements from the document <head>"
  []
  (let [styles (.getElementsByTagName js/document "style")
        style-count (.-length styles)]
    (doseq [n (range style-count 0 -1)]
      (.remove (aget styles (dec n))))))


(defn mount-style
  "Mount the style-element into the header with `style-text`, ensuring this is the only `<style>` in the doc"
  [style-text]
  (let [head (or (.-head js/document)
                 (aget (.getElementsByTagName js/document "head") 0))
        style-el (doto (.createElement js/document "style")
                   (-> .-type (set! "text/css"))
                   (.appendChild (.createTextNode js/document style-text)))]
    (.appendChild head style-el)))
