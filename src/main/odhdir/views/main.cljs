(ns odhdir.views.main
  "Our main view (index) page, including introduction and an aggregation of the XML feeds of my four primary blogs."
  (:require [reagent.core :as r]
            [clojure.string :as str]
            [ajax.core :as ax :refer [GET]]
            [goog.string :as gstr]
            [goog.Uri.QueryData :as qd]))

(def NUM-TRIES (r/atom 6))
(def WORD-LENGTH (r/atom 5))

(defn title-bar [] 
  [:h1 {:class "heading"} "Wordle"])

(defn words-grid []
  [:h2 "Words grid"]
  (into [:div.words-grid 
         (for [nt (range @NUM-TRIES)] 
           (into [:div.row 
                  (for [wl (range @WORD-LENGTH)]
                    [:div.letter-box {:style {"border" "1px solid black"
                                              "width" "1em"
                                              "height" "1em"
                                              "display" "inline-block"
                                              "margin" "5px"
                                              "padding" "2px"}}])])
           )])
  )

(defn keyboard-display []
  [:h2 "keyboard display"])
(defn odhdir
  "Front-page view"
  []
  [:div
   [title-bar]
   [words-grid]
   [keyboard-display]]) 
