(ns odhdir.views.message
  "In other apps useful for notifcation messages, though I don't need these here.
  Mounted by core into its own element."
  (:require [reagent.core :as r]))

(def MESSAGE
  "To trigger the rendering of a notification message,
  simply `reset!` something into this atom."
  (r/atom nil))

(defn clear-message
  "Call this to remove a message."
  []
  (when @MESSAGE
    (reset! MESSAGE nil)))

(def default-styles
  ["is-info" "is-light"])

(defonce MESSAGE-CLASSES (r/atom default-styles))

(defn message-view
  "The message area to display the content put into the app-db message"
  []
  (when-let [m @MESSAGE] 
    [:div.message.notification {:class @MESSAGE-CLASSES} m]))


