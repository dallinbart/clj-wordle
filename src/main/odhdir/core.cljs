(ns odhdir.core
  (:require [reagent.core :as r]
            [odhdir.views.styles :as style]
            [odhdir.views.main :as main]))

(defn mount-components []
  (r/render-component [#'main/odhdir] (.getElementById js/document "odh-directory")))

(defn init! []
  (style/mount-style (style/odhdir))
  (mount-components))
