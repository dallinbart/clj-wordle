#+DATE: <2021-09-09 Thu>

* ODH Directory
Clojurescript one-page app served by Wordpress.

* Technology
Created with pure Clojurescript using [[https://shadow-cljs.org/][ShadowCLJS]] and [[https://holmsand.github.io/reagent/][Reagent]].

# Styles with [[https://bulma.io/][Bulma]] and [[https://fontawesome.com/][Font Awesome]].

* To Start
Begin both front-end and back-end REPL (I use Cider; the backend is necessary for the code watcher) and then take a browser to localhost on the port in shadow-cljs :dev-http, which I have set to 8080. http://localhost:8080 . 

* License

©2021 BYU Office of Digital Humanities, Licensed with [[https://www.gnu.org/licenses/gpl-3.0.en.html][GPLv3]]
